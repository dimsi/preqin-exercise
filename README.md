# preqin-exercise

ETL solution which merges input tables (company data) into one in order to create
single json documents per company.

## My steps

1. Create virtual environment using `venv` module
1. Create git repo on gitlab & clone it
1. Call `poetry init` to get clean `pyproject.toml`
1. Prep essentials like `pre-commit`, install `mypy`, `black`, etc..
1. Write schemas in `schemas.py` so I have idea what my goal
1. Code ...

## Installation

Either using `poetry`:

```bash
poetry install
```

Or using `venv` + `pip`:

```bash
python -m vevn .venv
source .venv/bin/activate
pip install -f requirements.txt
```

## Usage

```
python run.py --help
```

Output json is saved to `./out/`. They are formated as a single text line to save space.

Using `--pretty-json` flag results in output as:

```json
{
    "name": "Competing Alt Investments",
    "addresses": [
        {
            "id": 3,
            "line1": "BBB King Street",
            "line2": "Berlin A3B4",
            "country": "Germany"
        }
    ],
    "asset_class": "Hedge Funds",
    "aum": 5,
    "currency": "USD"
}
```

## TO-DO

1. drop pandas manipulation in favor of `sqlalchemy` + `pydantic` schemas
    - needs to be tested whats faster

## Remarks

Every json is validated against `schemas.Output`.
