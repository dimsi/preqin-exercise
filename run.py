import sys
from pathlib import Path
from typing import Optional
from typing import Sequence

import structlog

import exercise.exercise

log = structlog.get_logger()
OUTDIR = "./out"


def main(argv: Optional[Sequence[str]] = None):
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--cores",
        default=1,
        type=int,
        help="Utilize multiple cores",
    )
    parser.add_argument(
        "--extra-rows",
        help="Generate extra rows to fake DB",
        type=int,
        default=0,
    )
    parser.add_argument(
        "--pretty-json",
        action="store_true",
        help="Indent nicely output json",
    )
    args = parser.parse_args(argv)
    log.info(args)

    Path(OUTDIR).mkdir(exist_ok=True)

    exercise.exercise.runner(
        extra_fake_data=args.extra_rows,
        pretty_json=args.pretty_json,
        processes=args.cores,
    )

    return 0


if __name__ == "__main__":
    sys.exit(main())
