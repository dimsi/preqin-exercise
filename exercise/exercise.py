import multiprocessing
import time
from pathlib import Path
from typing import Any
from typing import Iterator

import pandas as pd
import structlog
from tqdm import tqdm

import exercise.db
import exercise.schemas

log = structlog.get_logger()


def transform_group_into_schema(group: pd.DataFrame) -> exercise.schemas.Firm:
    """
    Reshape and validate data for single firm.

    "group" dataframe must hold same values in "flat_cols" unlike "nested_cols",
    where different data per row are expected.

    Args:
        group (pd.DataFrame): Groupby obj/dataframe with expected columns:
            [
                "firm_id",
                "firm_name",
                "asset_class_id",
                "aum_mn",
                "firm_currency",
                "id_asset",
                "asset_class",
                "address_id",
                "line_1",
                "line_2",
                "country_id",
                "country_name",
            ]

    Returns:
        Output: Validated data in pydantic schema ready for json dump
    """
    # as some data in schema are flat and some nested, separate them up-front
    flat_cols = [
        "firm_name",
        "asset_class",
        "aum_mn",
        "firm_currency",
    ]
    nested_cols = [
        "address_id",
        "line_1",
        "line_2",
        "country_name",
    ]

    df: pd.DataFrame = group[["firm_id"] + flat_cols + nested_cols]
    """  # noqa
    Example of "df":
                    firm_name  address_id                 line_1      line_2 country_name     asset_class  aum_mn firm_currency
    0     Large investment Co           1  123 Investment Street  Paris 1234       France  Private Equity    1241           EUR
    1     Large investment Co           2          10 Second Rue  Paris 1234       France  Private Equity    1241           EUR
    """

    flat = df[flat_cols].iloc[0].to_dict()
    nested = df[nested_cols].to_dict(orient="records")
    """
    Example of "flat":
        {'firm_name': 'Large investment Co', 'asset_class': 'Private Equity', 'aum_mn': 1241, 'firm_currency': 'EUR'}

    Example of "nested":
        [
            {'address_id': 1, 'line_1': '123 Investment Street', 'line_2': 'Paris 1234', 'country_name': 'France'},
            {'address_id': 2, 'line_1': '10 Second Rue', 'line_2': 'Paris 1234', 'country_name': 'France'},
        ]
    """

    # glue together payload for pydantic schema
    payload: dict[str, Any] = {}
    payload.update(flat)
    payload.update({"addresses": nested})

    # load & validate payload against schema
    out = exercise.schemas.Firm(**payload)

    return out


def join_tables(db: exercise.schemas.Db) -> pd.DataFrame:
    """
    Merge all input tables into one.

    As there is no SQL database + output schema, it must be done using pandas only.

    Args:
        db (Db): Pydantic obj which holds all input tables.

    Returns:
        pd.DataFrame: All data combined in a dataframe with cols:
            [
                "firm_id",
                "firm_name",
                "asset_class_id",
                "aum_mn",
                "firm_currency",
                "id_asset",
                "asset_class",
                "address_id",
                "line_1",
                "line_2",
                "country_id",
                "country_name",
            ]
    """

    # 1. add asset_class strings
    df = pd.merge(
        db.firms,
        db.asset_class,
        left_on="asset_class_id",
        right_on="id",
        suffixes=("_firm", "_asset"),
    ).rename(columns={"id_firm": "firm_id"})

    # 2. add address link
    df = pd.merge(
        df,
        db.firm_addresses,
    )

    # 3. add actual address(es) + country_id
    df = pd.merge(
        df,
        db.addresses,
        left_on="address_id",
        right_on="id",
    ).drop(columns=["id"])
    # drop col "id" as its same as "address_id"

    # 4. add country name
    df = pd.merge(
        df,
        db.countries,
        left_on="country_id",
        right_on="id",
    ).drop(columns=["id"])
    # drop "id" as its same as "country_id"

    return df


def worker(group: tuple[int, pd.DataFrame]) -> exercise.schemas.Firm:
    _, group = group
    return transform_group_into_schema(group)


def runner(
    extra_fake_data: int = 0,
    pretty_json: bool = False,
    processes: int = 1,
) -> Path:
    """Package entrypoint"""

    if processes > 1:
        cores = multiprocessing.cpu_count()
        assert processes <= cores, f"Cant use more than {cores} cores"

    # either use init data or add extra fake ones
    db: exercise.schemas.Db = exercise.db.get_db(extra_rows=extra_fake_data)

    start = time.monotonic()
    log.info("start")

    df = join_tables(db=db)
    g = df.groupby("firm_id")

    if processes > 1:
        with multiprocessing.Pool(processes=processes) as pool:
            lazy_jsons: Iterator = pool.imap(worker, g, chunksize=100)

            firms: list[exercise.schemas.Firm] = []
            for i in tqdm(
                lazy_jsons,
                total=len(df["firm_id"].unique()),
                desc="Generating JSON's",
            ):
                firms.append(i)

        output = exercise.schemas.Output(firms=firms)

    else:
        # container for all resulting json's
        output = exercise.schemas.Output()
        for _, group in tqdm(g, desc="Generating JSON's"):
            firm: exercise.schemas.Firm = transform_group_into_schema(group=group)
            output.firms.append(firm)

    # save locally
    outpath = Path("out") / "out.json"
    out_json: str = output.to_json(pretty=pretty_json)  # json format in text
    outpath.write_text(out_json)

    log.info(
        "output saved",
        path=str(outpath),
        took=time.monotonic() - start,
        n=len(output.firms),
    )

    return outpath


if __name__ == "__main__":
    runner()
