"""
Fake DB in form of pandas dataframes.
"""
import random

import pandas as pd
from faker import Faker
from tqdm import tqdm

import exercise.schemas

country_choices = ["France", "Spain", "Italy", "Germany"]
asset_choices = [
    "Private Equity",
    "Private Debt",
    "Natural Resources",
    "Infrastructure",
    "Hedge Funds",
    "Real Estate",
]

addresses_dic: dict = {
    "id": [1, 2, 3],
    "line_1": ["123 Investment Street", "10 Second Rue", "BBB King Street"],
    "line_2": ["Paris 1234", "Paris 1234", "Berlin A3B4"],
    "country_id": [1, 1, 4],
}
firm_addresses_dic: dict = {
    "firm_id": [1, 1, 2, 3],
    "address_id": [1, 2, 2, 3],
}
countries_dic: dict = {
    "id": list(range(1, 5)),
    "country_name": country_choices,
}
asset_class_dic: dict = {
    "id": list(range(1, 7)),
    "asset_class": asset_choices,
}
firms_dic: dict = {
    "id": list(range(1, 4)),
    "firm_name": [
        "Large investment Co",
        "Investment Co subsidiary",
        "Competing Alt Investments",
    ],
    "asset_class_id": [1, 2, 5],
    "aum_mn": [1241, 2, 5],
    "firm_currency": ["EUR", "EUR", "USD"],
}


def get_db(extra_rows: int = 0) -> exercise.schemas.Db:
    """
    Consolidate input testing data into one class.

    Data can be extended by fake extra data, using param "extra_rows".
    All randomness here is seeded, so it gives always same results.

    Args:
        extra_rows (int, optional): How many rows to generate. Extra row doesnt always
                                    mean extra firm, as it might just be secondary
                                    address for same firm. Defaults to 0.

    Returns:
        exercise.schemas.Db: Obj, where each attr represents one dataframe
    """
    assert extra_rows >= 0, "Arg extra_rows must be positive"

    if extra_rows:
        random.seed(11)
        fake = Faker()
        fake.seed_instance(11)

        # init fake data
        firm_name = fake.name()
        firm_id = 1
        asset_class_id = random.choice(range(1, len(asset_choices) + 1))
        aum_mn = random.randint(1, 1_000_000_000)
        currency = random.choice(["EUR", "USD"])

        for i in tqdm(range(1, extra_rows + 1), desc="Fake DB"):
            addr1, addr2 = fake.address().split("\n")
            addresses_dic["id"].append(3 + i)
            addresses_dic["line_1"].append(addr1)
            addresses_dic["line_2"].append(addr2)
            addresses_dic["country_id"].append(
                random.choice(range(1, len(country_choices) + 1))
            )

            if random.randint(1, 10) < 2:
                firm_name = firm_name
                firm_id = firm_id
                asset_class_id = asset_class_id
                aum_mn = aum_mn
                currency = currency
            else:
                firm_name = fake.name()
                firm_id = 3 + i
                asset_class_id = random.choice(range(1, len(asset_choices) + 1))
                aum_mn = random.randint(1, 1_000)
                currency = random.choice(["EUR", "USD"])
                firms_dic["id"].append(firm_id)
                firms_dic["firm_name"].append(firm_name)
                firms_dic["asset_class_id"].append(asset_class_id)
                firms_dic["aum_mn"].append(aum_mn)
                firms_dic["firm_currency"].append(currency)

            # this is a link table, must be always filled
            firm_addresses_dic["firm_id"].append(firm_id)
            firm_addresses_dic["address_id"].append(3 + i)

    db = exercise.schemas.Db(
        addresses=pd.DataFrame.from_dict(addresses_dic),
        firm_addresses=pd.DataFrame.from_dict(firm_addresses_dic),
        countries=pd.DataFrame.from_dict(countries_dic),
        asset_class=pd.DataFrame.from_dict(asset_class_dic),
        firms=pd.DataFrame.from_dict(firms_dic),
    )

    return db
