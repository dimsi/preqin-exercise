import json
from enum import Enum

import orjson
import pandas as pd
from pydantic import BaseModel
from pydantic.json import pydantic_encoder


class Db(BaseModel):
    """Container for DataFrames, acts kinda like a fake DB"""

    addresses: pd.DataFrame
    asset_class: pd.DataFrame
    countries: pd.DataFrame
    firm_addresses: pd.DataFrame
    firms: pd.DataFrame

    class Config:
        """Allow schema without validation for this special case"""

        arbitrary_types_allowed = True


class AssetClass(Enum):
    private_equity = "Private Equity"
    private_debt = "Private Debt"
    natural_resources = "Natural Resources"
    infrastructure = "Infrastructure"
    hedge_funds = "Hedge Funds"
    real_estate = "Real Estate"


class Currency(Enum):
    EUR = "EUR"
    USD = "USD"


class Address(BaseModel):
    id: int
    line1: str
    line2: str
    country: str

    class Config:
        """Translation dict so we dont have to rename DataFrame columns"""

        fields = {
            "id": "address_id",
            "line1": "line_1",
            "line2": "line_2",
            "country": "country_name",
        }


class Firm(BaseModel):
    """
    Desired schema
        [
            {
                "name": "",
                "addresses": [{
                    "id": "",
                    "line1": "",
                    "line2": "",
                    "country": ""
                }, ...],
                "assetclass": "",
                "aum": 1234,
                "currency": ""
            },
            {
                ...
            },
        ]
    """

    name: str
    addresses: list[Address]
    asset_class: AssetClass
    aum: int
    currency: Currency

    class Config:
        """Translation dict so we dont have to rename DataFrame columns"""

        fields = {
            "name": "firm_name",
            "aum": "aum_mn",
            "currency": "firm_currency",
        }


def orjson_firms_dumps(v, *, default):
    # orjson.dumps returns bytes, to match standard json.dumps we need to decode
    return orjson.dumps(v["firms"], default=default).decode()


class Output(BaseModel):
    firms: list[Firm] = []

    def json_pretty(self) -> str:
        """Slower, but nicer json"""
        return json.dumps(self.firms, indent=4, default=pydantic_encoder)

    def to_json(self, pretty: bool = False) -> str:
        if pretty:
            return self.json_pretty()
        else:
            return self.json()

    class Config:
        json_loads = orjson.loads
        json_dumps = orjson_firms_dumps
