import pandas as pd
import pydantic
import pytest

# fixtures (not using pytest.fixture for simplicity)

dict_with_single_company_row = {
    "index": [0],
    "firm_id": [1],
    "firm_name": ["Google inc."],
    "asset_class_id": [1],
    "aum_mn": [123_456_789],
    "firm_currency": ["USD"],
    "id_asset": [1],
    "asset_class": ["Infrastructure"],
    "address_id": [1],
    "line_1": ["Addr1"],
    "line_2": ["Addr2"],
    "country_id": [1],
    "country_name": ["USA"],
}
dict_with_multiple_company_rows = {
    "index": [0, 1, 2],
    "firm_id": [1] * 3,
    "firm_name": ["Google inc."] * 3,
    "asset_class_id": [1] * 3,
    "aum_mn": [123_456_789] * 3,
    "firm_currency": ["USD"] * 3,
    "id_asset": [1] * 3,
    "asset_class": ["Infrastructure"] * 3,
    "address_id": [1, 2, 3],
    "line_1": ["Addr1-1", "Addr1-2", "Addr1-3"],
    "line_2": ["Addr2-2", "Addr2-2", "Addr2-3"],
    "country_id": [1, 1, 2],
    "country_name": ["USA", "Canada", "GB"],
}

# tests


@pytest.mark.parametrize(
    "df_dic",
    (dict_with_single_company_row, dict_with_multiple_company_rows),
)
def test_success_transform(df_dic):
    from exercise.exercise import transform_group_into_schema

    df = pd.DataFrame(df_dic)
    out = transform_group_into_schema(df)

    # 1. test flat column
    # == Google inc.
    assert df_dic.get("firm_name")[0] == out.dict(include={"name"}).get("name")

    # 2. test nested column(s)
    # == [{'country': 'USA'}, {'country': 'Canada'}, {'country': 'GB'}]
    assert [{"country": x} for x in df_dic.get("country_name")] == out.dict(
        include={"addresses": {"__all__": {"country"}}}
    ).get("addresses")


@pytest.mark.parametrize(
    "df_dic",
    (dict_with_single_company_row, dict_with_multiple_company_rows),
)
def test_failing_transform(df_dic):
    from exercise.exercise import transform_group_into_schema

    df = pd.DataFrame(df_dic)
    df.loc[0, "firm_currency"] = "CZK"  # cant be validated
    with pytest.raises(pydantic.ValidationError):
        transform_group_into_schema(df)

    df = pd.DataFrame(df_dic)
    df.loc[0, "asset_class"] = "sum string"  # cant be validated
    with pytest.raises(pydantic.ValidationError):
        transform_group_into_schema(df)


def test_success_join_tables():
    from exercise.db import get_db
    from exercise.exercise import join_tables

    db = get_db()
    df = join_tables(db)

    expected_cols = [
        "firm_id",
        "firm_name",
        "asset_class_id",
        "aum_mn",
        "firm_currency",
        "id_asset",
        "asset_class",
        "address_id",
        "line_1",
        "line_2",
        "country_id",
        "country_name",
    ]

    # havent we lost some cols?
    assert all([col in df.columns for col in expected_cols])

    # db has 3 company IDs on 4 rows
    assert df["firm_id"].to_list() == [1, 1, 2, 3]

    # all companies come from 2 countries
    assert all(
        [country in df["country_name"].unique() for country in ["France", "Germany"]]
    )


def test_failing_join_tables():
    from exercise.db import get_db
    from exercise.exercise import join_tables

    db = get_db()

    # put non-existent country_id in table - it cant be joined
    db.addresses.loc[0, "country_id"] = 10
    df = join_tables(db)

    assert df.shape[0] != 4  # shall be 4 rows, but its broken coz of country_id = 10
    assert df.shape[0] == 3  # 1 row got lost

    # db has 3 company IDs on 4 rows
    assert df["firm_id"].to_list() == [1, 2, 3]
