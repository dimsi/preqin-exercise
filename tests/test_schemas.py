import pydantic
import pytest

from exercise.schemas import Firm


@pytest.mark.parametrize(
    "currency,asset_class",
    (
        ["EUR", "some string"],  # will fail on "some string" - not in Enum
        ["CZK", "Private Equity"],  # will fail on "CZK" - not in Enum
    ),
)
def test_failing_currency_and_asset_validation(currency, asset_class):
    """Test if schema handles not-allowed (not in schemas.Currency+AssetClass Enums"""
    dic = {
        "firm_name": "sum firm",
        "addresses": [
            {
                "address_id": 1,
                "line_1": "address line 1",
                "line_2": "address line 2",
                "country_name": "czech republic",
            }
        ],
        "asset_class": asset_class,
        "aum_mn": 1234,
        "firm_currency": currency,
    }
    with pytest.raises(pydantic.ValidationError):
        Firm(**dic)


@pytest.mark.parametrize(
    "currency,asset_class",
    (
        ["EUR", "Private Equity"],
        ["USD", "Hedge Funds"],
    ),
)
def test_success_currency_and_asset_validation(currency, asset_class):
    """Both currency + assets shall pass, as Enum names are used"""
    dic = {
        "firm_name": "sum firm",
        "addresses": [
            {
                "address_id": 1,
                "line_1": "address line 1",
                "line_2": "address line 2",
                "country_name": "czech republic",
            }
        ],
        "asset_class": asset_class,
        "aum_mn": 1234,
        "firm_currency": currency,
    }
    Firm(**dic)
