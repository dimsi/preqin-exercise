import os

import pytest


def test_run_too_many_cores():
    from run import main

    cores = os.cpu_count()
    too_many = cores * 10

    with pytest.raises(AssertionError):
        main(["--cores", str(too_many)])
